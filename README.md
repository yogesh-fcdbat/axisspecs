# AxisSpecs #

This repository contains the private CocoaPods specifications of all axis bank (DBAT) iOS libraries/frameworks.

### 1. Add AxisSpec Private Spec Repo to your CocoaPods installation

	```console
	$ pod repo add fcdebat https://yogesh-fcdbat@bitbucket.org/yogesh-fcdbat/axisspecs.git
	```

### 2. Add Source at top of Project Podfile dependent on Axis Libraries

	```console
	source 'https://yogesh-fcdbat@bitbucket.org/yogesh-fcdbat/axisspecs.git'
	source 'https://github.com/CocoaPods/Specs.git'
	```